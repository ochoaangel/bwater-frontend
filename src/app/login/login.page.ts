import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(public platform: Platform, public router: Router) { }

  ngOnInit() { }

  btnIniciarSesion() {
    this.router.navigateByUrl('/dashboard');
  }

}
