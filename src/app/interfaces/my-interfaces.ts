
export interface Producto {
    _id: string
    codigo: string,
    nombre: string,
    tipo: string,
    stock: number,
    unidad: string,
    cantidad: number,
    imagen: string,
    sucursal: string,
    clasificacion: string,
    descripcion: string,
    status: string,
    proveedores: string,
    costo: number,
    cantidadxpaquete: number,
    precio: number,
    comision: number,
    impuesto: number
}

export interface Pedido { 
    _id: string
    codigo: string,
    estado: string,
    id_producto: string,
    cantidad: number,
    fecha_de_compra: string,
    fecha_de_entrega: string,
    fecha_de_edición: string,
    fecha_de_eliminación: string,
    origen_gps: number,
    destino_gps: number,
    origen_dirección: string,
    destinod_dirección: string,
    id_conductor: string,
    id_destinatario: string,
    status: string,
}
