import { Injectable } from '@angular/core';
import { ActivationEnd, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ServiceService {


  public singlePage = ['login', 'register']

  constructor(public router: Router, public toastController: ToastController) { }

  getRoute() {
    return this.router.events.pipe(
      filter(evento => evento instanceof ActivationEnd),
      filter((evento: ActivationEnd) => evento.snapshot.url.length > 0),
      map((evento: ActivationEnd) => evento.snapshot.routeConfig.path)
    )
  }

  async mostrarToast(message) {
    const toast = await this.toastController.create({
      duration: 2000,
      message,
      animated: true,
      color: 'primary',
    });
    toast.present();
  }

}
