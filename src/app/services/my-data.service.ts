import { Injectable } from '@angular/core';
import { Pedido, Producto } from '../interfaces/my-interfaces';

@Injectable({
  providedIn: 'root'
})
export class MyDataService {

  constructor() { }

  productosAll: Producto[] = [
    {
      _id: '0365rge32rh163gn513fgj57',
      codigo: 'N-12365',
      nombre: 'Botellón 19Lts',
      tipo: 'BotellónTipo',
      stock: 10,
      unidad: 'gal',
      cantidad: 5,
      imagen: 'imagen',
      sucursal: 'Charallave',
      clasificacion: 'BotellonClasificacion',
      descripcion: 'BotellonDescripción',
      status: 'activo',
      proveedores: 'BotellónProveedor',
      costo: 124,
      cantidadxpaquete: 2,
      precio: 200,
      comision: 5,
      impuesto: 16,
    },
    {
      _id: '689d7gfdf5b454y35sdf3b54',
      codigo: 'N-8521',
      nombre: 'Tapa Grande de Botellón 19Lts',
      tipo: 'TapaGrande',
      stock: 54,
      unidad: 'pieza',
      cantidad: 1,
      imagen: 'TapaImagen',
      sucursal: 'Cúa',
      clasificacion: 'Tapa plástica',
      descripcion: 'Tapa Grande de Botellón 19Lts',
      status: 'activo',
      proveedores: 'EmpresaTapaProveedor',
      costo: 5,
      cantidadxpaquete: 50, 
      precio: 6,
      comision: 1,
      impuesto: 16
    },
    {
      _id: '689d7gfdf5b45y35sdf3b54',
      codigo: 'N-8591',
      nombre: 'Agua 5L Pet',
      tipo: 'Agua Pet',
      stock: 120,
      unidad: 'L',
      cantidad: 5,
      imagen: 'AguaImagen',
      sucursal: 'Ocumare',
      clasificacion: 'Agua Pet',
      descripcion: 'Agua 5L',
      status: 'activo',
      proveedores: 'AguaEmpresaProveedor',
      costo: 8,
      cantidadxpaquete: 50,
      precio: 7,
      comision: 1,
      impuesto: 16
    },
    {
      _id: '689d7gfdf5b44y35sdf3b54',
      codigo: 'N-8521',
      nombre: 'Agua 355ml Pet',
      tipo: 'TapaTipo',
      stock: 54,
      unidad: 'gal',
      cantidad: 1,
      imagen: 'TapaImagen',
      sucursal: 'Charallave',
      clasificacion: 'Agua Pet ml',
      descripcion: 'Agua 355ml Pet',
      status: 'activo',
      proveedores: 'Agua',
      costo: 14,
      cantidadxpaquete: 10,
      precio: 16,
      comision: 4,
      impuesto: 16
    }
  ]

  pedidosAll: Pedido[] = [
    {
      _id: '2342454sg6d5g4c6b5sb',
      codigo: 'N-25478',
      estado: 'En proceso',
      id_producto: '0365rge32rh163gn513fgj57',
      cantidad: 5,
      fecha_de_compra: '',
      fecha_de_entrega: '',
      fecha_de_edición: '',
      fecha_de_eliminación: '',
      origen_gps: 0,
      destino_gps: 0,
      origen_dirección: 'Calle 30',
      destinod_dirección: 'Ciudad 5',
      id_conductor: '787654gws6er8gdf7g6',
      id_destinatario: '85446dgsb63546f',
      status: 'activo',
    },
    {
      _id: '2342454sg6d5g4c6b5sb',
      codigo: 'N-25478',
      estado: 'En proceso',
      id_producto: '0365rge32rh163gn513fgj57',
      cantidad: 5,
      fecha_de_compra: '',
      fecha_de_entrega: '',
      fecha_de_edición: '',
      fecha_de_eliminación: '',
      origen_gps: 0,
      destino_gps: 0,
      origen_dirección: 'Calle 30',
      destinod_dirección: 'Ciudad 5',
      id_conductor: '787654gws6er8gdf7g6',
      id_destinatario: '85446dgsb63546f',
      status: 'activo',
    }
  ]



  getListaPedidos() {

  }

}
