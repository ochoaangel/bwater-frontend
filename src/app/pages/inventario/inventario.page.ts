import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalProductComponent } from 'src/app/components/modal-product/modal-product.component';
import { ServiceService } from 'src/app/services/myservice.service';

@Component({
  selector: 'app-inventario',
  templateUrl: './inventario.page.html',
  styleUrls: ['./inventario.page.scss'],
})
export class InventarioPage implements OnInit {

  constructor(private modalController: ModalController, private myservice: ServiceService) { }

  productModalProp;
  productModalDataReturned;


  productosAll = [
    // { codigo: '##1', nombre: '--1--', tipo: '__1__', stock: 8, unidad: 'xxxxxxx', cantidad: 4, imagen: 'xxxxxxx', sucursal: 'xxxxxxx', clasificacion: 'xxxxxxx', descripcion: 'xxxxxxx', status: 'xxxxxxx', proveedores: 'xxxxxxx', costo: 5, cantidadxpaquete: 7, precio: 30, comision: 20, impuesto: 16 },
    // { codigo: '##2', nombre: '--2--', tipo: '__2__', stock: 8, unidad: 'xxxxxxx', cantidad: 4, imagen: 'xxxxxxx', sucursal: 'xxxxxxx', clasificacion: 'xxxxxxx', descripcion: 'xxxxxxx', status: 'xxxxxxx', proveedores: 'xxxxxxx', costo: 5, cantidadxpaquete: 7, precio: 30, comision: 20, impuesto: 16 },
    // { codigo: '##3', nombre: '--3--', tipo: '__3__', stock: 8, unidad: 'xxxxxxx', cantidad: 4, imagen: 'xxxxxxx', sucursal: 'xxxxxxx', clasificacion: 'xxxxxxx', descripcion: 'xxxxxxx', status: 'xxxxxxx', proveedores: 'xxxxxxx', costo: 5, cantidadxpaquete: 7, precio: 30, comision: 20, impuesto: 16 },
    // { codigo: '##4', nombre: '--4--', tipo: '__4__', stock: 8, unidad: 'xxxxxxx', cantidad: 4, imagen: 'xxxxxxx', sucursal: 'xxxxxxx', clasificacion: 'xxxxxxx', descripcion: 'xxxxxxx', status: 'xxxxxxx', proveedores: 'xxxxxxx', costo: 5, cantidadxpaquete: 7, precio: 30, comision: 20, impuesto: 16 },
    // { codigo: '##5', nombre: '--5--', tipo: '__5__', stock: 8, unidad: 'xxxxxxx', cantidad: 4, imagen: 'xxxxxxx', sucursal: 'xxxxxxx', clasificacion: 'xxxxxxx', descripcion: 'xxxxxxx', status: 'xxxxxxx', proveedores: 'xxxxxxx', costo: 5, cantidadxpaquete: 7, precio: 30, comision: 20, impuesto: 16 },
    // { codigo: '##6', nombre: '--6--', tipo: '__6__', stock: 8, unidad: 'xxxxxxx', cantidad: 4, imagen: 'xxxxxxx', sucursal: 'xxxxxxx', clasificacion: 'xxxxxxx', descripcion: 'xxxxxxx', status: 'xxxxxxx', proveedores: 'xxxxxxx', costo: 5, cantidadxpaquete: 7, precio: 30, comision: 20, impuesto: 16 },
    // { codigo: '##7', nombre: '--7--', tipo: '__7__', stock: 8, unidad: 'xxxxxxx', cantidad: 4, imagen: 'xxxxxxx', sucursal: 'xxxxxxx', clasificacion: 'xxxxxxx', descripcion: 'xxxxxxx', status: 'xxxxxxx', proveedores: 'xxxxxxx', costo: 5, cantidadxpaquete: 7, precio: 30, comision: 20, impuesto: 16 },
    // { codigo: '##8', nombre: '--8--', tipo: '__8__', stock: 8, unidad: 'xxxxxxx', cantidad: 4, imagen: 'xxxxxxx', sucursal: 'xxxxxxx', clasificacion: 'xxxxxxx', descripcion: 'xxxxxxx', status: 'xxxxxxx', proveedores: 'xxxxxxx', costo: 5, cantidadxpaquete: 7, precio: 30, comision: 20, impuesto: 16 },
    // { codigo: '##9', nombre: '--9--', tipo: '__9__', stock: 8, unidad: 'xxxxxxx', cantidad: 4, imagen: 'xxxxxxx', sucursal: 'xxxxxxx', clasificacion: 'xxxxxxx', descripcion: 'xxxxxxx', status: 'xxxxxxx', proveedores: 'xxxxxxx', costo: 5, cantidadxpaquete: 7, precio: 30, comision: 20, impuesto: 16 },
    // { codigo: '##a', nombre: '--a--', tipo: '__a__', stock: 8, unidad: 'xxxxxxx', cantidad: 4, imagen: 'xxxxxxx', sucursal: 'xxxxxxx', clasificacion: 'xxxxxxx', descripcion: 'xxxxxxx', status: 'xxxxxxx', proveedores: 'xxxxxxx', costo: 5, cantidadxpaquete: 7, precio: 30, comision: 20, impuesto: 16 },
    // { codigo: '##b', nombre: '--b--', tipo: '__b__', stock: 8, unidad: 'xxxxxxx', cantidad: 4, imagen: 'xxxxxxx', sucursal: 'xxxxxxx', clasificacion: 'xxxxxxx', descripcion: 'xxxxxxx', status: 'xxxxxxx', proveedores: 'xxxxxxx', costo: 5, cantidadxpaquete: 7, precio: 30, comision: 20, impuesto: 16 },
    // { codigo: '##c', nombre: '--c--', tipo: '__c__', stock: 8, unidad: 'xxxxxxx', cantidad: 4, imagen: 'xxxxxxx', sucursal: 'xxxxxxx', clasificacion: 'xxxxxxx', descripcion: 'xxxxxxx', status: 'xxxxxxx', proveedores: 'xxxxxxx', costo: 5, cantidadxpaquete: 7, precio: 30, comision: 20, impuesto: 16 },
    // { codigo: '##d', nombre: '--d--', tipo: '__d__', stock: 8, unidad: 'xxxxxxx', cantidad: 4, imagen: 'xxxxxxx', sucursal: 'xxxxxxx', clasificacion: 'xxxxxxx', descripcion: 'xxxxxxx', status: 'xxxxxxx', proveedores: 'xxxxxxx', costo: 5, cantidadxpaquete: 7, precio: 30, comision: 20, impuesto: 16 },
    // { codigo: '##e', nombre: '--e--', tipo: '__e__', stock: 8, unidad: 'xxxxxxx', cantidad: 4, imagen: 'xxxxxxx', sucursal: 'xxxxxxx', clasificacion: 'xxxxxxx', descripcion: 'xxxxxxx', status: 'xxxxxxx', proveedores: 'xxxxxxx', costo: 5, cantidadxpaquete: 7, precio: 30, comision: 20, impuesto: 16 },
    // { codigo: '##f', nombre: '--f--', tipo: '__f__', stock: 8, unidad: 'xxxxxxx', cantidad: 4, imagen: 'xxxxxxx', sucursal: 'xxxxxxx', clasificacion: 'xxxxxxx', descripcion: 'xxxxxxx', status: 'xxxxxxx', proveedores: 'xxxxxxx', costo: 5, cantidadxpaquete: 7, precio: 30, comision: 20, impuesto: 16 },
    // { codigo: '##g', nombre: '--g--', tipo: '__g__', stock: 8, unidad: 'xxxxxxx', cantidad: 4, imagen: 'xxxxxxx', sucursal: 'xxxxxxx', clasificacion: 'xxxxxxx', descripcion: 'xxxxxxx', status: 'xxxxxxx', proveedores: 'xxxxxxx', costo: 5, cantidadxpaquete: 7, precio: 30, comision: 20, impuesto: 16 },
    // { codigo: '##h', nombre: '--h--', tipo: '__h__', stock: 8, unidad: 'xxxxxxx', cantidad: 4, imagen: 'xxxxxxx', sucursal: 'xxxxxxx', clasificacion: 'xxxxxxx', descripcion: 'xxxxxxx', status: 'xxxxxxx', proveedores: 'xxxxxxx', costo: 5, cantidadxpaquete: 7, precio: 30, comision: 20, impuesto: 16 },
  ]

  ngOnInit() {
  }

  async presentModal(myprops?) {

    let allData = {
      component: ModalProductComponent,
      backdropDismiss: false,
    }

    myprops ? allData['componentProps'] = { mydata: myprops } : null;
    this.productModalProp = await this.modalController.create(allData);

    this.productModalProp.onDidDismiss().then((dataReturned) => {

      switch (dataReturned.data?.action) {
        case 'eliminar':
          this.productosAll.splice(myprops.i, 1)
          this.myservice.mostrarToast('Producto Eliminado');
          break;

        case 'guardar':
          this.productosAll.splice(myprops.i, myprops.producto)
          this.myservice.mostrarToast('Producto Guardado');
          break;

        case 'nuevo':
          this.productosAll = [...this.productosAll, dataReturned.data.myData.producto]
          this.myservice.mostrarToast('Agregado producto');
          break;

        default:
          console.log('no se eliminó ni se guardò nada')
          break;
      }


      if (dataReturned !== null) {
        this.productModalDataReturned = dataReturned.data;
      }

    });

    return await this.productModalProp.present();
  }


  handleProduct(i?, producto?) {
    if ( i >= 0 && !!producto ) {
      let data = { i, producto }
      this.presentModal(data)
    } else {
      this.presentModal();
    }
  }

}
