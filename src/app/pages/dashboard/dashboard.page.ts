import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  
  slideOptionsProducts = {
    slidesPerView: 5,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    }
  }

  pedidosAll = [
    { fecha: '14/02/21', id: 'N-4546876', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '15', estado: 'En Proceso', conductor: 'Ruben Duran' },
    { fecha: '2/03/21', id: 'N-5546878', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '20', estado: 'Entregado', conductor: 'Mario Morales' },
    { fecha: '3/05/20', id: 'N-1146855', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '08', estado: 'Regresado', conductor: 'Marco Ortiz' },
   
    { fecha: '14/02/21', id: 'N-4546876', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '15', estado: 'En Proceso', conductor: 'Ruben Duran' },
    { fecha: '2/03/21', id: 'N-5546878', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '20', estado: 'Entregado', conductor: 'Mario Morales' },
    { fecha: '3/05/20', id: 'N-1146855', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '08', estado: 'Regresado', conductor: 'Marco Ortiz' },
    { fecha: '14/02/21', id: 'N-4546876', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '15', estado: 'En Proceso', conductor: 'Ruben Duran' },
    { fecha: '2/03/21', id: 'N-5546878', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '20', estado: 'Entregado', conductor: 'Mario Morales' },
    { fecha: '3/05/20', id: 'N-1146855', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '08', estado: 'Regresado', conductor: 'Marco Ortiz' },
    { fecha: '14/02/21', id: 'N-4546876', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '15', estado: 'En Proceso', conductor: 'Ruben Duran' },
    { fecha: '2/03/21', id: 'N-5546878', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '20', estado: 'Entregado', conductor: 'Mario Morales' },
    { fecha: '3/05/20', id: 'N-1146855', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '08', estado: 'Regresado', conductor: 'Marco Ortiz' },
    { fecha: '14/02/21', id: 'N-4546876', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '15', estado: 'En Proceso', conductor: 'Ruben Duran' },
    { fecha: '2/03/21', id: 'N-5546878', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '20', estado: 'Entregado', conductor: 'Mario Morales' },
    { fecha: '3/05/20', id: 'N-1146855', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '08', estado: 'Regresado', conductor: 'Marco Ortiz' },
    { fecha: '14/02/21', id: 'N-4546876', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '15', estado: 'En Proceso', conductor: 'Ruben Duran' },
    { fecha: '2/03/21', id: 'N-5546878', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '20', estado: 'Entregado', conductor: 'Mario Morales' },
    { fecha: '3/05/20', id: 'N-1146855', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '08', estado: 'Regresado', conductor: 'Marco Ortiz' },
    { fecha: '14/02/21', id: 'N-4546876', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '15', estado: 'En Proceso', conductor: 'Ruben Duran' },
    { fecha: '2/03/21', id: 'N-5546878', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '20', estado: 'Entregado', conductor: 'Mario Morales' },
    { fecha: '3/05/20', id: 'N-1146855', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '08', estado: 'Regresado', conductor: 'Marco Ortiz' },
    { fecha: '14/02/21', id: 'N-4546876', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '15', estado: 'En Proceso', conductor: 'Ruben Duran' },
    { fecha: '2/03/21', id: 'N-5546878', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '20', estado: 'Entregado', conductor: 'Mario Morales' },
    { fecha: '3/05/20', id: 'N-1146855', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '08', estado: 'Regresado', conductor: 'Marco Ortiz' },
    { fecha: '14/02/21', id: 'N-4546876', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '15', estado: 'En Proceso', conductor: 'Ruben Duran' },
    { fecha: '2/03/21', id: 'N-5546878', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '20', estado: 'Entregado', conductor: 'Mario Morales' },
    { fecha: '3/05/20', id: 'N-1146855', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '08', estado: 'Regresado', conductor: 'Marco Ortiz' },
    { fecha: '14/02/21', id: 'N-4546876', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '15', estado: 'En Proceso', conductor: 'Ruben Duran' },
    { fecha: '2/03/21', id: 'N-5546878', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '20', estado: 'Entregado', conductor: 'Mario Morales' },
    { fecha: '3/05/20', id: 'N-1146855', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '08', estado: 'Regresado', conductor: 'Marco Ortiz' },
    { fecha: '14/02/21', id: 'N-4546876', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '15', estado: 'En Proceso', conductor: 'Ruben Duran' },
    { fecha: '2/03/21', id: 'N-5546878', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '20', estado: 'Entregado', conductor: 'Mario Morales' },
    { fecha: '3/05/20', id: 'N-1146855', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '08', estado: 'Regresado', conductor: 'Marco Ortiz' },
    { fecha: '14/02/21', id: 'N-4546876', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '15', estado: 'En Proceso', conductor: 'Ruben Duran' },
    { fecha: '2/03/21', id: 'N-5546878', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '20', estado: 'Entregado', conductor: 'Mario Morales' },
    { fecha: '3/05/20', id: 'N-1146855', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '08', estado: 'Regresado', conductor: 'Marco Ortiz' },
    { fecha: '14/02/21', id: 'N-4546876', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '15', estado: 'En Proceso', conductor: 'Ruben Duran' },
    { fecha: '2/03/21', id: 'N-5546878', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '20', estado: 'Entregado', conductor: 'Mario Morales' },
    { fecha: '3/05/20', id: 'N-1146855', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '08', estado: 'Regresado', conductor: 'Marco Ortiz' },
    { fecha: '14/02/21', id: 'N-4546876', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '15', estado: 'En Proceso', conductor: 'Ruben Duran' },
    { fecha: '2/03/21', id: 'N-5546878', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '20', estado: 'Entregado', conductor: 'Mario Morales' },
    { fecha: '3/05/20', id: 'N-1146855', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '08', estado: 'Regresado', conductor: 'Marco Ortiz' },
    { fecha: '14/02/21', id: 'N-4546876', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '15', estado: 'En Proceso', conductor: 'Ruben Duran' },
    { fecha: '2/03/21', id: 'N-5546878', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '20', estado: 'Entregado', conductor: 'Mario Morales' },
    { fecha: '3/05/20', id: 'N-1146855', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '08', estado: 'Regresado', conductor: 'Marco Ortiz' },
    { fecha: '14/02/21', id: 'N-4546876', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '15', estado: 'En Proceso', conductor: 'Ruben Duran' },
    { fecha: '2/03/21', id: 'N-5546878', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '20', estado: 'Entregado', conductor: 'Mario Morales' },
    { fecha: '3/05/20', id: 'N-1146855', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '08', estado: 'Regresado', conductor: 'Marco Ortiz' },
    { fecha: '14/02/21', id: 'N-4546876', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '15', estado: 'En Proceso', conductor: 'Ruben Duran' },
    { fecha: '2/03/21', id: 'N-5546878', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '20', estado: 'Entregado', conductor: 'Mario Morales' },
    { fecha: '3/05/20', id: 'N-1146855', tipo: 'Botella de Agua', modelo: 'Botellon', sucursal: 'N21', stock: '08', estado: 'Regresado', conductor: 'Marco Ortiz' },
  ]

  constructor() { }

  ngOnInit() {
  }

}
