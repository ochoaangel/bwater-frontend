import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DistribuidoresPageRoutingModule } from './distribuidores-routing.module';

import { DistribuidoresPage } from './distribuidores.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DistribuidoresPageRoutingModule,
    ComponentsModule
  ],
  declarations: [DistribuidoresPage]
})
export class DistribuidoresPageModule {}
