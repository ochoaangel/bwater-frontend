import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalPedidoComponent } from 'src/app/components/modal-pedido/modal-pedido.component';
import { ServiceService } from 'src/app/services/myservice.service';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.page.html',
  styleUrls: ['./pedidos.page.scss'],
})
export class PedidosPage implements OnInit {

  constructor(private modalController: ModalController, private myservice: ServiceService) { }

  pedidoModalProp;
  pedidoModalDataReturned;
  pedidosAll;

  public lat = 24.799448;
  public lng = 120.979021;
  
  public origin: any;
  public destination: any; 
  
  ngOnInit() {
    this.origin = { lat: 24.799448, lng: 120.979021 };
    this.destination = { lat: 24.799524, lng: 120.975017 };
  
    // Location within a string
    // this.origin = 'Taipei Main Station';
    // this.destination = 'Taiwan Presidential Office';
  }



  async presentModal(myprops?) {

    let allData = {
      component: ModalPedidoComponent,
      backdropDismiss: false,
    }

    myprops ? allData['componentProps'] = { mydata: myprops } : null;
    this.pedidoModalProp = await this.modalController.create(allData);

    this.pedidoModalProp.onDidDismiss().then((dataReturned) => {

      console.log('dataReturned', dataReturned)

      switch (dataReturned.data?.action) {
        case 'eliminar':
          this.pedidosAll.splice(myprops.i, 1)
          this.myservice.mostrarToast('Producto Eliminado');
          break;

        case 'guardar':
          this.pedidosAll.splice(myprops.i, myprops.producto)
          this.myservice.mostrarToast('Producto Guardado');
          break;

        case 'nuevo':
          this.pedidosAll = [...this.pedidosAll, dataReturned.data.myData.producto]
          this.myservice.mostrarToast('Agregado producto');
          break;

        default:
          console.log('no se eliminó ni se guardò nada')
          break;
      }


      if (dataReturned !== null) {
        this.pedidoModalDataReturned = dataReturned.data;
      }

    });

    return await this.pedidoModalProp.present();
  }


  handlePedido(i?, producto?) {
    if (i >= 0 && !!producto) {
      let data = { i, producto }
      this.presentModal(data)
    } else {
      this.presentModal();
    }
  }



}
