import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { ModalFacturacionComponent } from 'src/app/components/modal-facturacion/modal-facturacion.component';
import { ModalPedidoComponent } from 'src/app/components/modal-pedido/modal-pedido.component';
import { ServiceService } from 'src/app/services/myservice.service';

@Component({
  selector: 'app-facturacion',
  templateUrl: './facturacion.page.html',
  styleUrls: ['./facturacion.page.scss'],
})
export class FacturacionPage implements OnInit {

  constructor(private modalController: ModalController, private myservice: ServiceService, private alertController: AlertController) { }

  pedidoModalProp;
  pedidoModalDataReturned;
  pedidosAll;

  ngOnInit() {
  }



  async presentModal(myprops?) {

    let allData = {
      component: ModalFacturacionComponent,
      // component: ModalPedidoComponent,
      backdropDismiss: false,
    }

    myprops ? allData['componentProps'] = { mydata: myprops } : null;
    this.pedidoModalProp = await this.modalController.create(allData);

    this.pedidoModalProp.onDidDismiss().then((dataReturned) => {

      console.log('dataReturned', dataReturned)

      switch (dataReturned.data?.action) {
        case 'eliminar':
          this.pedidosAll.splice(myprops.i, 1)
          this.myservice.mostrarToast('Producto Eliminado');
          break;

        case 'guardar':
          this.pedidosAll.splice(myprops.i, myprops.producto)
          this.myservice.mostrarToast('Producto Guardado');
          break;

        case 'nuevo':
          this.pedidosAll = [...this.pedidosAll, dataReturned.data.myData.producto]
          this.myservice.mostrarToast('Agregado producto');
          break;

        default:
          console.log('no se eliminó ni se guardò nada')
          break;
      }


      if (dataReturned !== null) {
        this.pedidoModalDataReturned = dataReturned.data;
      }

    });

    return await this.pedidoModalProp.present();
  }


  handlePedido(i?, producto?) {
    if (i >= 0 && !!producto) {
      let data = { i, producto }
      this.presentModal(data)
    } else {
      this.presentModal();
    }
  }


  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirm!',
      message: 'Message <strong>text</strong>!!!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }




}
