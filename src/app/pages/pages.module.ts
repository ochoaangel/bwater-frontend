import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PagesPage } from './pages.page';
import { PagesPageRoutingModule } from './pages-routing.module';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PagesPageRoutingModule,
    AgmCoreModule,
    AgmDirectionModule
  ],
  declarations: [PagesPage]

})

export class PagesPageModule { } 
