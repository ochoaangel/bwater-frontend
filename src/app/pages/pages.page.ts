import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../services/myservice.service';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.page.html',
  styleUrls: ['./pages.page.scss'],
})
export class PagesPage implements OnInit {

  public appPages = [
    { title: 'Dashboard', icon: 'apps-outline', url: '/dashboard' },
    { title: 'Facturación', icon: 'receipt-outline', url: '/facturacion' },
    { title: 'Inventario', icon: 'file-tray-outline', url: '/inventario' },
    { title: 'Pedidos', icon: 'navigate-circle-outline', url: '/pedidos' },
    { title: 'Compras', icon: 'card-outline', url: '/compras' },
    { title: 'Transacciones', icon: 'earth-outline', url: '/transacciones' },
    { title: 'Matrices', icon: 'storefront-outline', url: '/sucursales' },
    { title: 'Distribuidores', icon: 'bus-outline', url: '/distribuidores' },
    { title: 'Usuarios', icon: 'person-circle', url: '/usuarios' },
    { title: 'Configuración', icon: 'settings-outline', url: '/configuracion' }
  ];

  constructor(private router: Router, public myservice: ServiceService) {}

  ngOnInit() { }

}
