import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/services/myservice.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  title = 'titulo'

  constructor(public myservice: ServiceService) {
    this.myservice.getRoute().subscribe(url => {
      let uno = url.substring(0, 1).toUpperCase();
      let dos = url.substring(1, url.length).toLowerCase();
      this.title = uno + dos;
    })
  }

  ngOnInit() { }

}
