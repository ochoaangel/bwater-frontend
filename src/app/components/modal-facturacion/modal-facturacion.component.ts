import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { Producto } from 'src/app/interfaces/my-interfaces';
import { MyDataService } from 'src/app/services/my-data.service';

@Component({
  selector: 'app-modal-facturacion',
  templateUrl: './modal-facturacion.component.html',
  styleUrls: ['./modal-facturacion.component.scss'],
})
export class ModalFacturacionComponent implements OnInit {



  myEmptyData;

  newPedido = false;

  myData = {
    i: 0,
    pedido: {
      codigo: 'N-23545345',
      nombre: 'Juan',
      apellido: 'Osorio',
      dir1: '',
      dir2: '',
      dir3: '',
      dir4: '',
      postal: '',
      productos: [
        { _id: '68g7fd6g8g6g7fg', nombre: 'Botellón 19Lts', cantidad: 1, precio: 56 },
        { _id: '242342353434534', nombre: 'Tapa Grande de Botellón 19Lts', cantidad: 6, precio: 14 },
        { _id: '68g7fd6g846g7fg', nombre: 'Agua 355ml Pet', cantidad: 3, precio: 6 },
        { _id: '242342353534534', nombre: 'Agua 5L Pet', cantidad: 8, precio: 4 },
      ]
    }
  }

  productos = [];
  productosFiltrados: Array<Producto> = [];


  productoEdit = { nombre: '', cantidad: 1, precio: 0 }
  productsTotal = 0
  productSelected: Producto;

  constructor(private modalController: ModalController,
    public myDataservice: MyDataService,
    private navParams: NavParams
  ) { }


  ngOnInit() {
    this.newPedido = true;
  }


  async closeModal(data?) {
    await this.modalController.dismiss(data);
  }

  guardarPedido() {
    this.myData.pedido.productos = this.productos
    console.log(this.myData)

    let data = { myData: this.myData }
    this.closeModal(data)
  }

  eliminarPedido() {
    this.closeModal({ action: 'eliminar' })
  }

  ionViewWillLeave() {
    this.newPedido = false;
  }

  productNameEditChange(texto: string) {
    if (texto.length === 0) {
      console.log("Nada que buscar..")
      this.productosFiltrados = [];
    } else if (texto.length > 0 && this.myDataservice.productosAll.length > 0) {
      this.productosFiltrados = this.myDataservice.productosAll.filter(x => x.nombre.toLocaleLowerCase().includes(texto.toLocaleLowerCase()))
      this.productSelected = null;
      this.productoEdit.precio = 0;
    }
  }

  productSelectedEdit(product: Producto) {
    this.productSelected = product;
    this.productoEdit.nombre = product.nombre;
    this.productosFiltrados = [];
    this.productCantidadEditChange(this.productoEdit.cantidad);
    console.log(this.productos)
  }

  productCantidadEditChange(cantidad) {
    if (cantidad === 0) {
      this.productoEdit.precio = 0;
    } else if (cantidad < 1 && this.productSelected) {
      console.log('Cantidad Inválida')
      this.productoEdit.cantidad = 1;
      this.productoEdit.precio = this.productSelected.precio;
    } else if (cantidad < 1 && !this.productSelected) {
      console.log('Cantidad Inválida')
      this.productoEdit.cantidad = 1;
      this.productoEdit.precio = 0;
    } else if (this.productSelected) {
      console.log('Cantidad Válida')
      this.productoEdit.precio = cantidad * this.productSelected.precio;
    }
  }

  addSelectedProduct() {
    if (this.productoEdit.precio !== 0) {
      this.productos.unshift({ cantidad: this.productoEdit.cantidad, producto: this.productSelected })
      this.productSelected = null;
      this.productoEdit.nombre = '';
      this.productoEdit.cantidad = 1;
      this.productoEdit.precio = 0;

      // calculo el total
      this.calculoProductsTotal()

    } else {
      console.log('caso de precio cero, no se agrega el producto')
    }
  }

  deleteProduct(i) {
    this.productos.splice(i, 1);
    this.calculoProductsTotal()

  }


  calculoProductsTotal() {
    this.productsTotal = 0;
    this.productos.forEach((element) => {
      this.productsTotal = this.productsTotal + (element.cantidad * element.producto.precio)
    });
    console.log(this.productos)
    console.log(this.productsTotal)
  }
}
