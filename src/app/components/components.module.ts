import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { ModalProductComponent } from './modal-product/modal-product.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListaDePedidosComponent } from './lista-de-pedidos/lista-de-pedidos.component';
import { ModalPedidoComponent } from './modal-pedido/modal-pedido.component';
import { IonicModule } from '@ionic/angular';
import { ModalFacturacionComponent } from './modal-facturacion/modal-facturacion.component';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';



@NgModule({
  declarations: [
    HeaderComponent,
    ModalProductComponent,
    ModalPedidoComponent,
    ModalFacturacionComponent,
    ListaDePedidosComponent,

  ],
  exports: [
    HeaderComponent,
    ModalProductComponent,
    ModalPedidoComponent,
    ModalFacturacionComponent,
    ListaDePedidosComponent,

  ],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, IonicModule, AgmCoreModule, AgmDirectionModule],
})
export class ComponentsModule { }
