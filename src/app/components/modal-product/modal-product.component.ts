import { Component, Input, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-modal-product',
  templateUrl: './modal-product.component.html',
  styleUrls: ['./modal-product.component.scss'],
})
export class ModalProductComponent implements OnInit {

  myData;

  newProduct = false;

  myEmptyData = {
    i: 0,
    producto: { codigo: '', nombre: '', tipo: '', stock: 0, unidad: '', cantidad: 0, imagen: '', sucursal: '', clasificacion: '', descripcion: '', status: '', proveedores: '', costo: 0, cantidadxpaquete: 0, precio: 0, comision: 0, impuesto: 0 },
  }


  constructor(private modalController: ModalController, private navParams: NavParams) { }


  ngOnInit() {
    if (!!this.navParams.data.mydata) {
      this.newProduct = false;
      this.myData = this.navParams.data.mydata;
    } else {
      this.newProduct = true;
      this.myData = this.myEmptyData;
    }
  }


  async closeModal(data?) {
    await this.modalController.dismiss(data);
  }

  guardarProducto() {
    let data = { myData: this.myData }
    if (this.newProduct) {
      data['action'] = 'nuevo'
    } else {
      data['action'] = 'guardar'
    }
    this.closeModal(data)
  }

  eliminarProducto() {
    this.closeModal({ action: 'eliminar' })
  }

  ionViewWillLeave() {
    this.newProduct = false;
  }

}
